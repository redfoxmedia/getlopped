<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// Local configuration
if (file_exists( dirname( __FILE__ ) . '/wp-config-local.php')) {

  define('WP_LOCAL_DEV', true);
  include(dirname( __FILE__ ) . '/wp-config-local.php');

}

// Remote configuration
else {
  // ** MySQL settings - You can get this info from your web host ** //
  /** The name of the database for WordPress */
  define('DB_NAME', 'glopper_2016');

  /** MySQL database username */
  define('DB_USER', 'glopper_redfox');

  /** MySQL database password */
  define('DB_PASSWORD', 'Studio1!@#');

  /** MySQL hostname */
  define('DB_HOST', 'localhost');
}

/** WP Local Dev environment 
//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/vagrant/web/getlopped/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('WP_LOCAL_DEV', false);*/

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-n3-q=%9g!VF29Si+3wpHQ_~9&rD{>Hk+S?wNMi/Rp-osOMTBH.^+VlZmbKV[)tr');
define('SECURE_AUTH_KEY',  'nSfkg,BK5!EAC]^aA?M+gv/pg7u;Cez)9O[r>l7Xp1EOqJ IlIBwguxKIw%}S*AY');
define('LOGGED_IN_KEY',    '8.`7XbGYw@^nu_R6 `e8Q}-B|xFq+/9$1*wSc%QE`!}Q`w*.&um+&s+*{n@5ia(H');
define('NONCE_KEY',        'Na&wc)(I)))*zPk|XUS|_eAu<&A(`;{v*-y[NdMV+]yR&eDEVt*uc,U#-cs+u0.|');
define('AUTH_SALT',        'Zd-R@iPZL,j07eTcRT++9%*N4:z*w+w?7 NvkB~qA?PojLu`S|Uyh$7Nt$t$;*,o');
define('SECURE_AUTH_SALT', '2^21.J.`L6wduOFm=Js6pIIlbe3TP?oZ2ST1)%+>o{(+5`8)G}3oMU4.VO(g+R_S');
define('LOGGED_IN_SALT',   'v|GM#dXQsK8)2A{(*+_GwTn+Ok0>3$xm28I{1<u)1`EPF;&th9)]x@Pvy(.<on;<');
define('NONCE_SALT',       'QQ5^oV3O/s;nuVe_;+mdn 3jW_@1i+S,iEFN^&+UJ$}ShJ2]>^KSP|zYYj07gJIx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/vagrant/web/getlopped/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
