
<header id="header" class="header">
	<div class="container">
		<a class="header__a" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
			<img class="header__img" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>">
		</a>
		<a href="tel:1800438567"><i class="fa fa-phone phone-mobile"></i></a>
		<i class="fa fa-bars header__menu-toggle" id="menuToggle"></i>
		<nav class="header__nav" id="menu">
			<?php
				if (has_nav_menu('primary_navigation')) :
					wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
				endif;
			?>
		</nav>
	</div>
</header>
