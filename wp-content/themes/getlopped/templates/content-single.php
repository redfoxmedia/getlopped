<?php


$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
if ( isset( $featured_image[0] ) ) {
  ?>
  <div class="container-fluid">
    <div class="bg-image" style="background-image: url(<?php echo esc_url( $featured_image[0] ); ?>);"></div>
  </div>
  <?php
}
?>
  <div class="container">
    <div class="row padding">
      <?php
      if ( have_posts() ) {
        while ( have_posts() ) {
          the_post();
          ?>
          <section>
            <div class="content">
               <header>
        <div>
          <?php the_title( '<h1>', '</h1>' ); ?>
        </div>
      </header>
              <p><?php echo wp_kses_post( apply_filters( 'the_content', $post->post_content ) ); ?></p>
             
            </div>
          </section>
          <aside>
                <div class="row margin-top">
                  <div class="prev">
                    <?php 
                    if ( get_previous_post()->ID && get_previous_post()->ID !== get_the_ID() ) {
                      ?>
                      <a class="button-prev"
                         href="<?php the_permalink( get_previous_post()->ID ) ?>">
                        <?php echo esc_html( 'Prev'); ?>
                      </a>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="next">
                    <?php

                    if ( get_next_post()->ID && get_next_post()->ID !== get_the_ID() ) {
                      ?>
                      <a class="button"
                         href="<?php the_permalink( get_next_post()->ID ); ?>">
                        <?php echo esc_html( 'Next'); ?>
                      </a>
                      <?php
                    }
                    ?>
                  </div>
                </div>
          </aside>
          <?php
        }
      }
      ?>
    </div>
  </div>
