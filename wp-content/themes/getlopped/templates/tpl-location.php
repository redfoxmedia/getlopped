<?php
/**
 * Template Name: Location
 */
?>

<section class="grey-bg">
	
	<div class="container container--1020 content padding center-headings">
		
		<span class="location__span">
		
			<?php if( get_field('intro') ) : ?>

				<?php the_field('intro'); ?>

			<?php endif; ?>	

		</span>

		<h2 class="typography__h2 typography__h2--green typography__h2--smaller">Call us now on <a href="tel:1800438567" class="typography__h2--a">1800 438 567</a></h2>

	</div>

</section>


<section>
	
	<div class="container content padding center-headings container--1020 ul--left">
		
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part('templates/content', 'page'); ?>
		
		<?php endwhile; ?>

	</div>

</section>


	<section class="grey-bg">
		
		<div class="container container--1020 content padding center-headings">
			
				<?php if( get_field('foot_note') ) : ?>

				<?php the_field('foot_note'); ?>

			<?php endif; ?>	
		</div>

	</section>
<?php if(get_field('breakout_image')) : ?>

<div class="breakout" style="background-image: url('<?php the_field('breakout_image'); ?>');"></div>

<?php endif; ?>
<br><br>
<?php get_template_part('parts/quotations'); ?>
