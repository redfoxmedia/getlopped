<?php
/**
 * Template Name: Service
 */
?>

<section class="banner banner--center banner--menu" style="background-image: url(<?php echo ( $banner = get_field( 'banner' ) ) ?  $banner : ''; ?>)">

	<nav class="service__nav">

		<h2 class="alt" id="serviceTitle">Tree Services</h2>

		<ul class="service__ul" id="serviceMenu">
	
			<?php if(function_exists('wp_nav_menu')) wp_nav_menu(array('container' => false, 'items_wrap' => '%3$s', 'theme_location' => 'tree-services')); ?>
			
		</ul>

	</nav>
	
</section>

<section class="container text-center content padding service__section">

	<?php if( have_rows('top_area') ) : ?>

		<?php while( have_rows('top_area') ) : the_row(); ?>
		
			<?php the_sub_field('content'); ?>
		
		<?php endwhile; ?>

	<?php endif; ?>

</section>

<section class="imagery flex">
	
	<?php if( have_rows('service_imagery') ) : ?>

		<?php while( have_rows('service_imagery') ) : the_row(); ?>
		
			<div class="imagery__div" style="background-image: url(<?php the_sub_field('image'); ?>);"></div>
		
		<?php endwhile; ?>

	<?php endif; ?>

</section>

<section class="content text-center container padding">
	
    <?php if( have_rows('tree_pruning_faq') ) : ?>
    	
    	<?php while( have_rows('tree_pruning_faq') ) : the_row(); ?>
        
            <div class="container">
                
                <?php the_sub_field('title'); ?>

            </div>
                
            <?php if( have_rows('list') ) : ?>
                
                <ul class="zebra flex flex--wrap styled__ul">

                    <?php while( have_rows('list') ) : the_row(); ?>

                        <li class="flext--item flex--half zebra__li">

                            <h3 class="typography__h2 typography__h2--green zebra__h2"><?php the_sub_field('title'); ?></h3>

                            <p><?php the_sub_field('content'); ?></p>

                        </li>

                    <?php endwhile; ?>

                </ul>

            <?php endif; ?>
     
		<?php endwhile; ?>

	<?php endif; ?>

</section>

<?php get_template_part('parts/quotations'); ?>