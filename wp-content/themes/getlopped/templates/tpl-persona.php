
<?php
/**
 * Template Name: Persona
 */
?>

<?php if ( $banner = get_field( 'banner' ) ) {
    $style = 'style="background-image:url(' .$banner. ');"';
} ?>

<section class="banner banner--center banner--menu" <?php echo $style; ?>>
	
	<nav class="service__nav">

		<h2 class="alt" id="serviceTitle">Tree Services</h2>

		<ul class="service__ul" id="serviceMenu">
	
			<?php if(function_exists('wp_nav_menu')) wp_nav_menu(array('container' => false, 'items_wrap' => '%3$s', 'theme_location' => 'tree-services')); ?>
			
		</ul>

	</nav>

</section>

<section class="container content padding text-center">
	
<?php the_field("content1"); ?>

</section>

<section class="content text-center container">
	
    <?php if( have_rows('customer_problem') ) : ?>
        
            <div class="container">
                
                <h2 class="alt"><?php the_field('top_title'); ?></h2>

            </div>
                
            <ul class="zebra flex flex--wrap styled__ul">

                <?php while( have_rows('customer_problem') ) : the_row(); ?>

                    <li class="flext--item flex--half zebra__li">

                        <h3 class="typography__h2 typography__h2--green zebra__h2"><?php the_sub_field('title'); ?></h3>

                        <p><?php the_sub_field('content'); ?></p>

                    </li>

                <?php endwhile; ?>

            </ul>

	<?php endif; ?>

</section>

<?php if(get_field('breakout_image')) : ?>

<div class="breakout" style="background-image: url('<?php the_field('breakout_image'); ?>');"></div>

<?php endif; ?>

<br><br>
<?php get_template_part('parts/quotations'); ?>