<footer class="footer">
	
	<div class="container flex footer__div--menu">	

		<div class="footer__div">
			
			<h3 class="typograghy__h3 footer__h3">Tree services</h3>

			<ul class="footer__ul">

				<?php if( have_rows('tree_services', 'options') ) : ?>

					<?php while( have_rows('tree_services', 'options') ) : the_row(); ?>
			    		
			    		<li class="footer__li">
			    		
			    			<a class="footer__a" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>

		    			</li>

			    	<?php endwhile; ?>

			    <?php endif; ?>

			</ul>

		</div>

		<div class="footer__div">
			
			<h3 class="typograghy__h3 footer__h3">Who we help</h3>

			<ul class="footer__ul">

				<?php if( have_rows('persona', 'options') ) : ?>

					<?php while( have_rows('persona', 'options') ) : the_row(); ?>
			    		
			    		<li class="footer__li">
			    		
			    			<a class="footer__a" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>

		    			</li>

			    	<?php endwhile; ?>

			    <?php endif; ?>

			</ul>

		</div>

		<div class="footer__div">
			
			<h3 class="typograghy__h3 footer__h3">Quick Links</h3>

			<ul class="footer__ul">

				<?php if( have_rows('quick_link', 'options') ) : ?>

					<?php while( have_rows('quick_link', 'options') ) : the_row(); ?>
			    		
			    		<li class="footer__li">
			    		
			    			<a class="footer__a" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('title'); ?></a>

		    			</li>

			    	<?php endwhile; ?>

			    <?php endif; ?>

			</ul>

		</div>

	</div>

	<div class="footer__div--copyright text-center">

		
			
<div class="footer__div--copyright text-center"><div itemscope itemtype="http://schema.org/LocalBusiness" >
   <span itemprop="name">Get Lopped Tree Services</span>
   <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
     <span itemprop="streetAddress">20 Clevedon Street</span>
     <span itemprop="addressLocality">Botany</span>,
     <span itemprop="addressRegion">NSW</span>
     <span itemprop="postalCode">2019</span>
     <span itemprop="addressCountry">Australia</span>
   </div>
   Phone: <span itemprop="telephone">1800 438 567</span>
</div></div>
<div class="footer-credit__redfox">
				<a href="http://www.redfoxmedia.com.au" target="_blank">
						Digital Agency Sydney
						<svg width="20px" height="19px" viewBox="0 0 20 19" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" role="img" aria-label="Redfox Media logo">
							<title>Redfox Media</title>
 							<desc>Redfox Media is a  Sydney based Digital Agency</desc>
						    <g id="Redfox-Credit" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						        <g id="Dark-Version" transform="translate(-267.000000, -95.000000)" fill="#D0D0CF">
						            <g id="Straight" transform="translate(97.000000, 94.000000)">
						                <g id="Off">
						                    <path d="M190,1.0014655 C189.9908,1.00224661 184,4.0014655 184,4.0014655 L176,4.0014655 L170,1.0014655 L172,9.0014655 L170,11.0014655 L176,15.0048227 L180,20 L184.01117,15.0048227 L189.999999,11.0014655 L187.999999,9.0014655 C187.999999,9.0014655 190.002994,1.00137871 190,1.0014655 Z" id="redfox-copy"></path>
						                </g>
						            </g>
						        </g>
						    </g>
						</svg>
					</a>
				</div>
	</div>
<?php if ($tracking_code = get_field('tracking_code', 'option')) echo $tracking_code; ?>


</footer>