<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
	<?php get_template_part('templates/head'); ?>
	<body <?php body_class(); ?> id="body">
		<!--[if IE]>
			<div class="alert alert-warning">
				<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
			</div>
		<![endif]-->
		<?php
			do_action('get_header');
			get_template_part('templates/header');
		?>
		<main class="main">	
			<!-- <div class="container">	 -->
			<?php include Wrapper\template_path(); ?>
			<!-- </div> -->
		</main>
		
		<aside class="side-menu">

			<ul class="side-menu__ul">

			    <li class="side-menu__li side-menu__li--contact">
			    
			    	<a href="/contact/" title="Contact Us" class="side-menu__a side-menu__a--contact"></a>
			    
			    </li>
			    
			    <li class="side-menu__li side-menu__li--call">

			    	<a href="tel:1800438567" title="Call Us" class="side-menu__a side-menu__a--call"></a>
			
			    	<span class="side-menu__span">call us: <a href="tel:1800438567" title="Call Us" class="side-menu__a--text"><strong>1800 438 567</strong></a></span>
			
			    </li>
			
			</ul>

		</aside>

		<?php
			do_action('get_footer');
			get_template_part('templates/footer');
			wp_footer();
		?>
	</body>
</html>
