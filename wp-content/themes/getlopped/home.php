<?php
/**
 * Template for blog index
 *
 */
$page_title = get_the_title( get_option( 'page_for_posts', true ) );
?>

	<div class="container">
		<div class="row padding">
			<div class="col-xs-12">
				<h1 class="text-center"><?php echo wp_kses_post( $page_title ); ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-9">
				
					<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
							if ( isset( $featured_image[0] ) ) {
								$style = 'background-image: url("' . $featured_image[0] . '");';
							} else {
								$style = 'background: #3b3b3b;';
							}
							?>
							<div class="col-xs-12">
								<article class="post-box equal">
									<div class="bg-image left-column"
									     style='<?php echo wp_kses_post( $style ); ?>'></div>
									<div class="right-column">
										<?php
										the_title( '<h2 class="typograghy__h3 footer__h3">', '</h2>' );
										echo '<p>';
										the_excerpt();
										echo '</p>';
										?>
									</div>
								</article>
							</div>
							<?php
						}
					}
					$link_to_blog = get_permalink( get_option( 'page_for_posts' ) );
					$page         = get_query_var( 'paged' ); // 0 or 1 for first page, 2 and next for other pages
					if ( 0 == $page ) {
						$page = 1;
					}
					$total_post_count     = wp_count_posts();
					$published_post_count = $total_post_count->publish;
					$total_pages          = intval( ceil( $published_post_count / $posts_per_page ) );
					?>
					<div class="col-xs-12">
						<div class="pagination">
							<?php
							if ( 1 !== $page ) {
								$p = $page - 1;
								?>
								<a href="<?php echo esc_url( $link_to_blog . 'page/' . $p ); ?>"
								   class="prev-btn"></a>
								<?php
							} else {
								?>
								<a class="prev-btn disable"></a>
								<?php
							}
							?>

							<form action="<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>" method="post"
							      autocomplete="off">
								<input type="text" id="number_of_page" name="id"
								       value="<?php echo esc_html( $page ); ?>"/>
								<input type="hidden" name="action" value="load_page"/>
							</form>
							<p><?php esc_html_e( 'of' ); ?></p>
							<a href="<?php echo esc_url( $link_to_blog . 'page/' . $total_pages ); ?>"><?php echo esc_html( $total_pages ); ?></a>

							<?php
							if ( $page !== $total_pages ) {
								$p = $page + 1;
								?>
								<a href="<?php echo esc_url( $link_to_blog . 'page/' . $p ); ?>"
								   class="next-btn"></a>
								<?php
							} else {
								?>
								<a class="next-btn disable"></a>
								<?php
							}
							?>
						</div>
					</div>
				
			</div>
			
		</div>
	</div>


