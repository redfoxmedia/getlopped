<?php get_template_part('templates/page', 'header'); ?>

<div class="container padding text-center content 404">
	
	<h1>404: Page not found</h1>

	<h2>You’re barking up the wrong tree!</h2>

	<p>The page you’re looking for couldn’t be found. You can try searching, or simply return <a href="/">home</a>.</p>

	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/tree.png" class="image" />

</div>
