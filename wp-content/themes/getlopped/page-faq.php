<?php $image = wp_get_attachment_url( get_post_thumbnail_id($post->ID)); ?>

<?php if($image) : ?>
	
	<section class="banner banner--height banner--center" style="background-image: url('<?php echo $image; ?>')"></section>

<?php endif; ?>

<section class="faq">
	
	<div class="container container--small content padding center-headings text-center">
		
		<h1 class="typography__h1"><?php the_title(); ?></h1>

		<?php the_content(); ?>

	</div>

</section>

<?php get_template_part('parts/quotations'); ?>
