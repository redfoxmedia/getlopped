
<?php  

	// Pages using location template

	$location_pages = get_posts(array(
		'post_type' => 'page',
		'meta_key' => '_wp_page_template',
		'meta_value' => 'templates/tpl-location.php'
	));

?>

<section class="grey-bg">
	
	<div class="container content padding-sml text-center content--dark container--grey-bg">
	
		<h1 class="typography__h1"><?php the_title(); ?></h1>
		
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part('templates/content', 'page'); ?>
		
		<?php endwhile; ?>
		<div role="tabpanel" class="tabs__div active leaf-list" id="what-we-do">

			<?php $index = 0; ?>

			<ul class="leaf-list__ul styled__ul leaf-list__ul--contact">

	        <?php foreach ($location_pages as $item) : ?>
	        	
				<li class="leaf-list__li <?php echo (++$index%2 === 1) ? 'leaf-list__li--left' : 'leaf-list__li--right'; ?>">

					<a href="/<?php echo $item->post_name; ?>/" class="leaf-list__a" title="<?php echo $item->post_title; ?>">

						<h3 class="typography__h3 leaf-list__h3"><?php echo $item->post_title; ?></h3>
				 
				 	</a>

				</li>
				
	         
	        <?php endforeach; ?>
				<li class="leaf-list__li <?php echo (++$index%2 === 1) ? 'leaf-list__li--left' : 'leaf-list__li--right'; ?>">

					<p class="leaf-list_nolink__a" title="Eastern Suburbs"><h3 class="typography__h3 leaf-list__h3">Eastern Suburbs</h3></p>

				</li>
	        </ul>

		</div>

	</div>

</section>

<!--<section class="grey-bg">
	
	<div class="container content padding-sml text-center content--dark  container--grey-bg" id="quote-form">
	
		<h2 class="typography__h2 alt">Request a Quote</h2>
		<h2 class="typography__h3">We’d love to hear from you. Fill out this form to request a quote or callback.</h2>

		<?php echo do_shortcode( '[contact-form-7 id="13" title="Contact form 1Get in touch"]' ); ?>

	</div>

</section>-->

<?php get_template_part('parts/quotations'); ?>

