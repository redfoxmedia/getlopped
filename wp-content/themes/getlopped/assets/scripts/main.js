/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.
	var Sage = {
		// All pages
		'common': {
			init: function() {

				// JavaScript to be fired on all page-specific

				$('.carousel').carousel({
					interval: false
				});

				var headerHeight = 148;
				var $header = $('#header');
				var $window = $(window);
				var $body = $('#body');

				$window.scroll(function(){

					if ($window.scrollTop() >= headerHeight) {

						$header.addClass('header--small');

					} else {

						$header.removeClass('header--small');

					}

				});

				// menu

				var $menu = $('#menu');
				var $menuToggle = $('#menuToggle');

				function toggleMenu(){
					$menuToggle.toggleClass('header__menu-toggle--active');
					$menu.fadeToggle();
					$body.toggleClass('menu-active');
				}

				$menuToggle.click(toggleMenu);

				// responsive dropdown

				var $menuLink = $('.menu-item a');
				var $menuItemActiveClass = 'menu-item--active';
				var $subMenuActiveClass = 'sub-menu--active';

				$menuLink.click(function(e){

					if($window.width() < 1020){
						
						var sibling = $(this).siblings('.sub-menu');

						if(sibling.length && !sibling.hasClass($subMenuActiveClass)){

							// remove previously clicked
							$('.' + $menuItemActiveClass).removeClass($menuItemActiveClass);
							$('.' + $subMenuActiveClass).removeClass($subMenuActiveClass).slideUp();

							// apply active
							$(this).toggleClass($menuItemActiveClass);
							sibling.addClass($subMenuActiveClass).slideDown();

							e.preventDefault();

						} else if(sibling.length) {
							
							$(this).toggleClass($menuItemActiveClass);
							sibling.removeClass($subMenuActiveClass).slideUp();
							e.preventDefault();

						}

					} else {

						$('.' + $menuItemActiveClass).removeClass($menuItemActiveClass);
						$(this).toggleClass($menuItemActiveClass);

					}

				}); 

				$('.wpcf7-file').change(function(){
					
					$('.placeholderv.file-name').val($(this).val());

				});

				// Service Menu

				var $serviceTitle = $('#serviceTitle'); 

				var $serviceMenu = $('#serviceMenu');

				$serviceTitle.click(function(){

					$(this).toggleClass('service__ul--active');
					$serviceMenu.slideToggle();

				});

				// Set active tab

				(function($){

					function openTab($tab){

						$tab.tab('show');

						var top = $(".tabs").offset().top - 140;

						$('html, body').animate({
							scrollTop: top
						}, 300);

					}

					var url = document.location.toString();

					if(url.match('#')){
						
						var $tab = $('.tabs__li a[href="#' + url.split('#')[1] + '"]');

						openTab($tab);

					}

					$menuLink.click(function(e){

						var $tab = $('.tabs__li a[href="#' + $(e.target).attr('href').split('#')[1]+ '"]');
					
						if($tab){
						
							openTab($tab);

							$('.current-menu-ancestor .sub-menu').css('display', 'none');

							setTimeout(function() {
								$('.current-menu-ancestor .sub-menu').removeAttr('style');							
							}, 500);

						} 

					});

				})(jQuery);

				// ready 

				$body.addClass('ready');

			},
			finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
			}
		},
		// Home page
		'home': {
			init: function() {
				// JavaScript to be fired on the home page
			},
			finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
			}
		},
		// About us page, note the change from about-us to about_us.
		'about_us': {
			init: function() {
				// JavaScript to be fired on the about us page
			}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			} 
		},
		loadEvents: function() {

			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
