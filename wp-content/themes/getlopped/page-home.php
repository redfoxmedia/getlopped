
<?php // Banner ?>

<?php if(have_rows('banner')) : ?>

	<?php while( have_rows('banner') ) : the_row(); ?>

		<div class="banner banner--home text-center" style="background-image: url('<?php the_sub_field('banner_image'); ?>');">

			<h1 class="banner__h1"><?php the_sub_field('banner_title'); ?></h1>

			<h2 class="banner__h2"><?php the_sub_field('banner_text'); ?></h2>

			<ul class="banner__ul">

			    <li class="banner__li banner__li--quote"><a class="banner__a" href="/contact/" title="Get a Quote">Get a quote <br class="desktop-br" />or make an enquiry</a></li>

			    <li class="banner__li banner__li--call"><a class="banner__a" href="tel:1800438567" title="Call Us">Call us: 1800 438 567</a></li>

			</ul>
		
		</div>

	<?php endwhile; ?>	

<?php endif; ?>

<?php // Intro ?>

<section class="intro text-center container">
	
	<div class="intro__div">
		
		<?php the_field('home_intro'); ?>

	</div>

</section>

<?php // What we do / Who we are ?>

<section class="tabs">

	<ul class="tabs__ul" role="tablist">
		
		<li role="presentation" class="tabs__li active">

			<a href="#what-we-do" class="tabs__a" aria-controls="what-we-do" role="tab" data-toggle="tab">What we do</a>

		</li>
		
		<li role="presentation" class="tabs__li">

			<a href="#who-we-are" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Who we are</a>

		</li>

	</ul>

	<div class="container">

		<div role="tabpanel" class="tabs__div active leaf-list styled__ul" id="what-we-do">

			<?php $solutions_index = 0; ?>

			<?php if( have_rows('our_tree_service_solutions') ) : ?>

				<ul class="leaf-list__ul">

	            <?php while( have_rows('our_tree_service_solutions') ) : the_row(); ?>
	            
					<li class="leaf-list__li <?php echo (++$solutions_index%2 === 1) ? 'leaf-list__li--left' : 'leaf-list__li--right'; ?>">

						<a href="<?php the_sub_field('link'); ?>" class="leaf-list__a" title="<?php the_sub_field('title'); ?>">

							<h3 class="typography__h3 leaf-list__h3"><?php the_sub_field('title'); ?></h3>

							<p class="leaf-list__p"><?php the_sub_field('text'); ?></p>
					 
					 	</a>

					</li>
	             
	            <?php endwhile; ?>

	            </ul>

	        <?php endif; ?>

		</div>

		<div role="tabpanel" class="tabs__div tabs__div--who-we-are" id="who-we-are"> 

			<div class="tabs__content tabs__content--left">
				
				<img class="tabs__img" src="<?php the_field('who_we_are_image'); ?>" alt="Who We Are" />

			</div>

			<div class="tabs__content tabs__content--right">
				
				<?php the_field('who_we_are_text'); ?>

				<a class="button tabs__a--content" href="/about-us/" title="About Us">Learn more about us</a>

			</div>

		</div>

	</div>

</section>

<?php // Services ?>


<section class="services">

	<?php if( have_rows('the_tree_service_specialists') ) : ?>

        <?php while( have_rows('the_tree_service_specialists') ) : the_row(); ?>

        	<div class="container text-center">

        		<?php the_sub_field('text'); ?>

			</div>

        	<div class="services__div">

	        	<ul class="container services__ul flex">
	           
	            	<?php if( have_rows('list') ) : ?>
	               
	                    <?php while( have_rows('list') ) : the_row(); ?>

							<li class="services__li flex--1 text-center">

								<a class="services__a" href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('title'); ?>">

								<?php $image = get_sub_field ('icon'); if( !empty($image) ): ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php endif; ?>

									<h3 class="typography__h3 services__h3"><?php the_sub_field('title'); ?></h3>

									<?php the_sub_field('text_area'); ?>

								</a>
					
							</li>

	                    <?php endwhile; ?>

	            	<?php endif; ?>

	            </ul>	

            </div>

            <div class="services__div--background" style="background-image: url('<?php the_sub_field('breakout_image'); ?>')"></div>

        <?php endwhile; ?>

    <?php endif;  ?>

</section>

<?php // Process ?>

<section class="process text-center">

	<?php if( have_rows('our_process') ) : ?>

		<?php while( have_rows('our_process') ) : the_row(); ?>

			<div class="container process__div">

				<?php the_sub_field('text'); ?>

			</div>

			<?php if( have_rows('list') ) : ?>

				<ul class="container process__ul flex">

					<?php while( have_rows('list') ) : the_row(); ?>

						<li class="process__li flex--1 text-center">
							
							<img class="process__img" <?php $image = get_sub_field ('icon'); if( !empty($image) ): ?>
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
<?php endif; ?>

							<h3 class="typography__h3"><?php the_sub_field('title'); ?></h3>

						</li>

					<?php endwhile; ?>

				<?php endif; ?>

	
		<?php endwhile; ?>

	<?php endif; ?>

</ul>		
		<p style="margin-top:40px">	<a class="button" href="/about-us/#process">Learn More</a></p>
		

</section>

<?php get_template_part('parts/quotations'); ?>

