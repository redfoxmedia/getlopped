


<section class="tabs about__tabs">

	<ul class="tabs__ul tabs__ul--about" role="tablist">
		
			<li role="presentation" class="tabs__li active">

			<a href="#about" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">About</a>

		</li>
		<li role="presentation" class="tabs__li">

			<a href="#our-people" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Our People</a>

		</li>
		
		<li role="presentation" class="tabs__li">

			<a href="#process" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Process</a>

		</li>

		<li role="presentation" class="tabs__li">

			<a href="#employment" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Employment</a>

		</li>

	</ul>

	<div role="tabpanel" class="tabs__div active" id="about"> 

		<?php if( have_rows('about_content') ) : ?>
						
								<div class="container text-center about__div about__div--intro content center-headings">
								<?php while( have_rows('about_content') ) : the_row(); ?>
					
								<?php the_sub_field('intro'); ?>
								</div>
								<div class="promise__div"><div class="container text-center about__div about__div--intro content center-headings"><?php the_sub_field('promise'); ?></div></div>


							<?php endwhile; ?>

						<?php endif; ?>


	</div>


	<div role="tabpanel" class="tabs__div" id="our-people">

		<?php if( have_rows('our_people') ) : ?>

			<?php while( have_rows('our_people') ) : the_row(); ?>

				<div class="container text-center about__div about__div--intro content center-headings">
					
					<?php the_sub_field('content'); ?></p>

				</div>
			
				<div class="breakout" style="background-image: url('<?php the_sub_field('image'); ?>');"></div>
			
			<?php endwhile; ?>

		<?php endif; ?>

		<div class="about__div about__div--grey">
			
			<div class="container">
				
				<ul class="tabs__ul" role="tablist">

					<li role="presentation" class="tabs__li active">

						<a href="#our-qualifications" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Our Qualifications</a>

					</li>
		
					<li role="presentation" class="tabs__li ">

						<a href="#our-teams" class="tabs__a" aria-controls="profile" role="tab" data-toggle="tab">Our Teams</a>

					</li>

				</ul>

				<div role="tabpanel" class="tabs__div active" id="our-qualifications">
					
					<div class="container carousel__div">

						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						
						<div class="carousel-inner" role="listbox">

							<?php if( have_rows('qualifications') ) : ?>

								<?php $first_slide = true; ?>

								<?php while( have_rows('qualifications') ) : the_row(); ?>
								
									<div class="item <?php echo ($first_slide) ? 'active' : ''; ?> text-center">

										<h2 class="typography__h2 carousel__h2"><?php the_sub_field('title'); ?></h2>

										<?php the_sub_field('left_content'); ?>

										<?php the_sub_field('right_content'); ?>

									</div>

									<?php $first_slide = false; ?>

								<?php endwhile; ?>

							<?php endif; ?>

							
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
						</a>
						</div>
						
						

					</div>

				</div>

				<div role="tabpanel" class="tabs__div" id="our-teams">

					<div class="about__div--team text-center">
						
						<?php if( have_rows('our_teams') ) : ?>
						
							<?php while( have_rows('our_teams') ) : the_row(); ?>
					
								<h2 class="typography__h2 carousel__h2"><?php the_sub_field('title'); ?></h2>
								<?php the_sub_field('content'); ?>

							<?php endwhile; ?>

						<?php endif; ?>

					</div>
					
				</div>

			</div>

		</div>

		<?php // Key Staff ?>

		<section class="staff">

			<div class="container about__div text-center">

				<h2 class="alt">Key staff</h2>

			</div>

			<?php if( have_rows('directors_&_employees') ) : ?>

				<ul class="container staff__ul flex">

					<?php while( have_rows('directors_&_employees') ) : the_row(); ?>
					
						<li class="staff__li flex--1">
							
							<h2 class="typography__h2 typography__h2--green staff__h2"><?php the_sub_field('name'); ?></h2>

							<div class="flex">
								
								<div class="staff__div staff__div--img">
									
									<img class="staff__img" <?php $image = get_sub_field ('avatar'); if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>

								</div>

								<div class="staff__div">

									<?php the_sub_field('content'); ?>

								</div>

							</div>

							
							

						</li>

					<?php endwhile; ?>	

				</ul>
			
			<?php endif; ?>

		</section>

	</div>

	<div role="tabpanel" class="tabs__div" id="process"> 

		<?php if( have_rows('our_process', 2) ) : ?>

			<?php while( have_rows('our_process', 2) ) : the_row(); ?>

		<div class="container container--1020 content text-center center-headings">

			<?php the_field('process_intro'); ?>

				</div>

					<?php if( have_rows('process') ) : ?>

					<ul class="container process__ul flex">

			<?php while( have_rows('process') ) : the_row(); ?>

							<li class="process__li flex--1 text-center">
								
							<img class="process__img" <?php $image = get_sub_field ('icon'); if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>

								<h3 class="typography__h3"><?php the_sub_field('title'); ?></h3>

								<p style="margin-left: 12px; margin-right: 12px"><?php the_sub_field('content'); ?></p>


							</li>
		
						<?php endwhile; ?>

					<?php endif; ?>

		
			<?php endwhile; ?>

		<?php endif; ?>

	</div>

	<div role="tabpanel" class="tabs__div" id="employment"> 

		<div class="container container--1020 content text-center center-headings">

			<?php the_field('employment_content'); ?>

		</div>

		<?php echo do_shortcode( '[contact-form-7 id="54" title="Employment"]' ); ?>

	</div>


</section>

<?php get_template_part('parts/quotations'); ?>