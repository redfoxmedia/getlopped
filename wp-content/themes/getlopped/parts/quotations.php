<section class="quotations">
	
	<div class="container flex">
		
		<div class="quotations__div quotations__div--left">
			
			<?php the_field('quotations', 'options'); ?>

			<a class="button" href="/contact/">Get a quote</a>

		</div>

		<div class="quotations__div quotations__div--right">

			<h2 class="typography__h2 typography__h2--dark quotations__h2">Get Lopped Tree Services <br class="desktop-br" />is compliant with</h2>
			
			<img class="quotations__img" src="<?php the_field('compliancy', 'options'); ?>" alt="Complicancy" />

		</div>

	</div>

</section>