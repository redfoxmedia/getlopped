<?php
/**
 * Featured image for page
 *
 * @package Collaroy
 */

$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
if ( isset( $featured_image[0] ) ) {
	?>
	<div class="container-fluid">
		<div class="bg-image" style="background-image: url(<?php echo esc_url( $featured_image[0] ); ?>);"></div>
	</div>
	<?php
}
